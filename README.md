# CodingChallenge | SAFEAUTO (TEST)

A cross platform (Windows) web application for make an upload file with ASP Core 3.1 and React.

# For Developers

### Contributing:

Any comments to improve this project are welcome.

### Prerequisites:

```
npm and nodejs
Asp Core
SQL Server 2016
```

1. Clone the solution [here](git@gitlab.com:fofojesus/safeautochallenge.git)

2. After clone, you must change the connectionString for your own credentials and database in this path:
```
safeautochallenge/src/02. Services/Kata/ChallengeSafeAuto.Kata.Api/appsentings.json
```

3. Open the project.
```
ChallengeSafeAuto.sln
```

4. Set this project as default:
**`ChallengeSafeAuto.Kata.Api`**

5. Type in the package manager console from visual studio console  the next command. You must Select this project 
**`ChallengeSafeAuto.Kata.Persistence.Database`**
```
update-datase
```
6. Run the webService in VisualStudio 

7. REACT -- Access in this path with the console in VS CLI.
```
safeautochallenge/src/01. Client/
```

8. Type the next commands in the terminal.
```
npm install
```
9. Run the web project.
```
npm start
```

10. Click in this link [here](http://localhost:8081/) to open the website



# WebSite
### This is an example
![](https://thumbs2.imgbox.com/2d/db/8K3APIwJ_t.jpg)

# FlatFile
### Example from flat file
![](https://thumbs2.imgbox.com/45/c0/BecGd9pk_t.jpg)
