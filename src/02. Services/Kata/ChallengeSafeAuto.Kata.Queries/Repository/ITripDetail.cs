﻿using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeSafeAuto.Kata.Service.Queries.Repository
{
    public interface ITripDetail
    {
        public Task<List<TripDetailDto>> GetAllTripDetail(int page, int take, IEnumerable<int> driversId = null);


        }
}
