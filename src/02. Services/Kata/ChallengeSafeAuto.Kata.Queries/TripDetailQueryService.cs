﻿using ChallengeSafeAuto.Kata.Persistence.Database;
using ChallengeSafeAuto.Kata.Service.Queries.Repository;
using Microsoft.EntityFrameworkCore;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeSafeAuto.Kata.Service.Queries
{
    public class TripDetailQueryService: ITripDetail
    {
        private readonly ApplicationDbContext _context;

        public TripDetailQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<TripDetailDto>> GetAllTripDetail(int page, int take, IEnumerable<int> driversId = null)
        {
            IQueryable<TripDetailDto> collection =  from driv in _context.Driver
                                join trip in _context.TripDetail                             
                             on driv.IdDriver    equals trip.IdDriver into dr
                             from drivers in dr.DefaultIfEmpty()
                             group  new { drivers.speed,drivers.TotalMinutes} 
                             by driv.DriverCar into grp
                             select new TripDetailDto { DriverName = grp.Key , milesDriven = grp.Sum(y => y.speed), mph = (((float)grp.Average(x=>x.speed) / (float)grp.Average(x => x.TotalMinutes) * 60) )};                            
            
            return  await collection.ToListAsync();
    }
    }
}
