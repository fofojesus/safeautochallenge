﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeSafeAuto.Kata.Service.Queries
{
    public class TripDetailDto
    {
       public string DriverName { get; set; }
        public float? milesDriven { get; set; }
        public float? mph { get; set; }                
    }
}
