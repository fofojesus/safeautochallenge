﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeSafeAuto.Kata.Domain
{
    public class TripDetail
    {
        public int IdTripDetail { get; set; }
        public TimeSpan startTrip { get; set; }
        public TimeSpan endTrip { get; set; }
        public float speed { get; set; }        
        public int IdDriver { get; set; }        
        public double? TotalMinutes { get; set; }

    }
}
