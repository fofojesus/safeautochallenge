﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChallengeSafeAuto.Kata.Service.EventHandlers.Commands;
using ChallengeSafeAuto.Kata.Service.Queries;
using ChallengeSafeAuto.Kata.Service.Queries.Repository;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;

namespace ChallengeSafeAuto.Kata.Api.Controllers
{
    [Route("v1/tripdetail")]
    [ApiController]
    public class DriverController : ControllerBase
    {

        private readonly ITripDetail _tripDetailQueryService;
        private readonly ILogger<DriverController> _logger;
        private readonly IMediator _mediator;
        

        public DriverController(
            ILogger<DriverController> logger,
            ITripDetail tripDetailQueryService,
            IMediator mediator
            )
        {
            _logger = logger;
            _mediator = mediator;
            _tripDetailQueryService = tripDetailQueryService;
        }

        [HttpGet]
        public async Task<List<TripDetailDto>> GetAll(int page = 1, int take = 1000, string driversId = null)
        {
            IEnumerable<int> ids = null;

            if (!string.IsNullOrEmpty(driversId))
            {
                ids = driversId.Split(',').Select(x => Convert.ToInt32(x));
            }

            return await _tripDetailQueryService.GetAllTripDetail(page, take, ids);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStock(TripDetailCreateCommand command)
        {
            await _mediator.Publish(command);
            return NoContent();
        }
    }
}
