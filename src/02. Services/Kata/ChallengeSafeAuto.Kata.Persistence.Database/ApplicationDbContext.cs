﻿using ChallengeSafeAuto.Kata.Domain;
using ChallengeSafeAuto.Kata.Persistence.Database.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeSafeAuto.Kata.Persistence.Database
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<TripDetail> TripDetail { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.HasDefaultSchema("Kata");
            ModelConfig(builder);
        }    

        public void ModelConfig(ModelBuilder modelBuilder)
        {
            new DriverConfiguration(modelBuilder.Entity<Driver>());
            new TripDetailConfiguration(modelBuilder.Entity<TripDetail>());
        }
    }
}
