﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ChallengeSafeAuto.Kata.Persistence.Database.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Kata");

            migrationBuilder.CreateTable(
                name: "Driver",
                schema: "Kata",
                columns: table => new
                {
                    IdDriver = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverCar = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Driver", x => x.IdDriver);
                });

            migrationBuilder.CreateTable(
                name: "TripDetail",
                schema: "Kata",
                columns: table => new
                {
                    IdTripDetail = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    startTrip = table.Column<TimeSpan>(type: "time", nullable: false),
                    endTrip = table.Column<TimeSpan>(type: "time", nullable: false),
                    speed = table.Column<float>(type: "real", nullable: false),
                    IdDriver = table.Column<int>(type: "int", nullable: false),
                    DriverIdDriver = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripDetail", x => x.IdTripDetail);
                    table.ForeignKey(
                        name: "FK_TripDetail_Driver_DriverIdDriver",
                        column: x => x.DriverIdDriver,
                        principalSchema: "Kata",
                        principalTable: "Driver",
                        principalColumn: "IdDriver",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TripDetail_DriverIdDriver",
                schema: "Kata",
                table: "TripDetail",
                column: "DriverIdDriver");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TripDetail",
                schema: "Kata");

            migrationBuilder.DropTable(
                name: "Driver",
                schema: "Kata");
        }
    }
}
