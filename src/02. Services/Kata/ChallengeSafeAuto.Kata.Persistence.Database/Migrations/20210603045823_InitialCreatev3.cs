﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChallengeSafeAuto.Kata.Persistence.Database.Migrations
{
    public partial class InitialCreatev3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TripDetail_Driver_DriverIdDriver",
                schema: "Kata",
                table: "TripDetail");

            migrationBuilder.DropIndex(
                name: "IX_TripDetail_DriverIdDriver",
                schema: "Kata",
                table: "TripDetail");

            migrationBuilder.DropColumn(
                name: "DriverIdDriver",
                schema: "Kata",
                table: "TripDetail");

            migrationBuilder.AlterColumn<double>(
                name: "TotalMinutes",
                schema: "Kata",
                table: "TripDetail",
                type: "float",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "TotalMinutes",
                schema: "Kata",
                table: "TripDetail",
                type: "float",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DriverIdDriver",
                schema: "Kata",
                table: "TripDetail",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TripDetail_DriverIdDriver",
                schema: "Kata",
                table: "TripDetail",
                column: "DriverIdDriver");

            migrationBuilder.AddForeignKey(
                name: "FK_TripDetail_Driver_DriverIdDriver",
                schema: "Kata",
                table: "TripDetail",
                column: "DriverIdDriver",
                principalSchema: "Kata",
                principalTable: "Driver",
                principalColumn: "IdDriver",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
