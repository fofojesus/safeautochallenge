﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ChallengeSafeAuto.Kata.Persistence.Database.Migrations
{
    public partial class InitialCreatev2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TotalMinutes",
                schema: "Kata",
                table: "TripDetail",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalMinutes",
                schema: "Kata",
                table: "TripDetail");
        }
    }
}
