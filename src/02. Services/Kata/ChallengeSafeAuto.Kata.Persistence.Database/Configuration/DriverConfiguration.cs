﻿using ChallengeSafeAuto.Kata.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeSafeAuto.Kata.Persistence.Database.Configuration
{
    public class DriverConfiguration
    {
        public DriverConfiguration(EntityTypeBuilder<Driver> entityBuilder)
        {
            entityBuilder.HasKey(x => x.IdDriver);
            entityBuilder.Property(x => x.DriverCar).IsRequired();            
        }
    }
}
