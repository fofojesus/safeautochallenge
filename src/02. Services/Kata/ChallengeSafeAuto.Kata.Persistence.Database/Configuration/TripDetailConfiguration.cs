﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using ChallengeSafeAuto.Kata.Domain;

namespace ChallengeSafeAuto.Kata.Persistence.Database.Configuration
{
   public class TripDetailConfiguration
    {
        public TripDetailConfiguration(EntityTypeBuilder<TripDetail> entityBuilder)
        {
            entityBuilder.HasKey(x => x.IdTripDetail);
            entityBuilder.Property(x => x.speed).IsRequired();
            entityBuilder.Property(x => x.startTrip).IsRequired();
            entityBuilder.Property(x => x.endTrip).IsRequired();
            entityBuilder.Property(x => x.TotalMinutes);
        }
    }
}
