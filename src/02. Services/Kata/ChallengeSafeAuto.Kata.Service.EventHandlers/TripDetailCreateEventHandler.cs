﻿using ChallengeSafeAuto.Kata.Domain;
using ChallengeSafeAuto.Kata.Persistence.Database;
using ChallengeSafeAuto.Kata.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChallengeSafeAuto.Kata.Service.EventHandlers
{
    class TripDetailCreateEventHandler : INotificationHandler<TripDetailCreateCommand>
    {
        private ApplicationDbContext _context;
        public TripDetailCreateEventHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(TripDetailCreateCommand notification, CancellationToken cancellationToken)
        {

            foreach (var item in notification.DriverItems)
            {
                if( _context.Driver.Where(x=>x.DriverCar == item.DriverCar).Count() == 0)
                {
                    await (_context.AddAsync(new Driver
                    {
                        DriverCar = item.DriverCar
                    }));
                }
                
            }
            await _context.SaveChangesAsync();

            foreach (var item in notification.Items)
            {
                var driver =  _context.Driver.Where(x => x.DriverCar == item.Driver).SingleOrDefault();

                TimeSpan startTime = TimeSpan.Parse(item.startTrip);
                TimeSpan endTime = TimeSpan.Parse(item.endTrip);

                double ts = endTime.Subtract(startTime).TotalMinutes; ;

                double avg = (item.speed / ts) * 60;
                if (driver != null && (avg> 5) && (avg <100))
                {
                    await (_context.AddAsync(new TripDetail
                    {
                        startTrip = startTime,
                        speed = item.speed,
                        endTrip = endTime,
                        IdDriver = driver.IdDriver,
                        TotalMinutes = ts


                    }));
                }                
            }           
            await _context.SaveChangesAsync();
        }
    }
}
