﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChallengeSafeAuto.Kata.Service.EventHandlers.Commands
{
    public class TripDetailCreateCommand: INotification
    {


        public IEnumerable<TripDetailCreateItem> Items { get; set; } = new List<TripDetailCreateItem>();
        public IEnumerable<DriverItem> DriverItems { get; set; } = new List<DriverItem>();
    }

    public class TripDetailCreateItem
    {     
        public string startTrip { get; set; }
        public string endTrip { get; set; }
        public float speed { get; set; }
        public string Driver { get; set; }
    }
    public class DriverItem
    {        
        public string DriverCar { get; set; }

    }
}
