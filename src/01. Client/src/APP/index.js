import React, { useState, useEffect, Fragment } from "react";
import { getTrip,postTrip } from "../Services/Controllers/kataController.js";
import uui from "uuid";

const App = () => {
  const [state, setState] = useState({
    trip: [],
  });

  useEffect(() => {
    getData();
  }, []);


  const  axiosConfig = {
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Access-Control-Allow-Origin": "*",
    },
  };

const   onLoad  =  (event) => {  
  var file = fileInput.files[0];
  var textType = /text.*/;

  if (file.type.match(textType)) {
      var reader = new FileReader();

      reader.onload = function(e) {
          var content = reader.result;          
          let resultData = content.split('\r\n');
          
          let info = {
            "Items":[],
            "DriverItems":[]
          }
          resultData.forEach(element => {
            let currentData = element.split(" ");
            if(currentData.length >0)
            {
              if(currentData[0]== "Driver" && currentData.length == 2)
              {
                info.DriverItems.push({"DriverCar": currentData[1]});
              }
              if(currentData[0]== "Trip" && currentData.length == 5){
                info.Items.push({"Driver": currentData[1],"startTrip": currentData[2].toString(),"endTrip": currentData[3].toString(),"speed": (currentData[4] *1 )});
              }
            }
           
          });
          
          postData(info)
          fileInput.value="";
      }

      reader.readAsText(file);    
  } 
}
  const postData =(data)=>
  {
    postTrip(data).then(x=>{
      getData();
    });
   
  }
  const getData = () => {
    
    getTrip("", axiosConfig).then((response) => {
      setState({
        ...state,
        trip: response.data,
      });
      //console.log(response.data);
    });
  };

  return (
    <div>
      <table>
        <thead>
          <tr><th colSpan="3"><input type="file" id="fileInput"  accept=".txt" onChange={(e) => onLoad(e)} /></th></tr>
          <tr>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {state.trip.length > 0 ? (
            <Fragment>
              {state.trip.map((x) => {
                return (
                  <tr >
                    <td><strong>{x.driverName} :</strong> </td>
                    <td>{x.milesDriven == null ? 0 : x.milesDriven.toFixed(0)} miles</td>
                    <td>@ {x.mph == null ? 0 : x.mph.toFixed(0)} mph</td>
                  </tr>
                );
              })}
            </Fragment>
          ) : null}
        </tbody>
      </table>
    </div>
  );
};
export default App;
