const IP = "localhost";
const IP_DEV = "localhost";
const URL = "";
const PROTOCOL = "http";
const PROTOCOL_SSL = "https";
const PORT = "10000";
const PORT_SSL = "5001";

 const  Configuracion = {
  /**
   * @description Retorna la base la url con la configuracion seteada
   * @param {boolean} isSSL Verica protocolo
   * @param {boolean} hasProtocol verifica si tiene protocolo´
   * @param {boolean} isDev verifica el entorno actual de trabajo
   * @returns {string}
   */
  getBaseURL: (isSSL=false, hasProtocol = false, isDev = false) => {
    return (`${isSSL ? PROTOCOL_SSL : PROTOCOL}://${(isDev ? IP_DEV : IP)}${ (hasProtocol ? (isSSL ? `:${PORT_SSL}` :`:${PORT}` ) : "")}`       
      
     
    );
  },

  getURLFull: (isSSL, method, hasProtocol = true, isDev = false) => {
    return (
      (isSSL ? PROTOCOL_SSL : PROTOCOL) +
      "://" +
      (isDev ? IP_DEV : IP) +
      (hasProtocol == 1 ? ":" + (isSSL ? PORT_SSL : PORT) : "") +
      "/" +
      method
    );
  },
};
export default Configuracion