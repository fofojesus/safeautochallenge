import axios from "axios";
import  ConfiguracionAPI  from '../config/configurationAPI'



axios.defaults.baseURL = ConfiguracionAPI.getBaseURL(false,true,true);

const requestGenerico = {
  get: (url) => axios.get(url),
  post: (url, body) => axios.post(url, body),
  put: (url, body) => axios.put(url, body),
  delete: (url) => axios.delete(url),
};

export default requestGenerico;
