import HttpCliente from "../HttpClient";


export const getTrip = (parameters,headers) => {
    return new Promise((resolve, eject) => {
      HttpCliente.get(`/v1/tripdetail/${parameters}`,headers).then((response) => {
        resolve(response);
      });
    });
  };


  export const postTrip = (data) => {
    return new Promise((resolve, reject ) => {
      HttpCliente.post(`/v1/tripdetail/`,data).then((response) => {
        resolve(response)
      })
    })
  }
  